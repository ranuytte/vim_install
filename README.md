# vim_install

## Install

```
git clone https://gitlab.com/ranuytte/vim_install.git ~/.vim_runtime
curl -fLo ~/.vim_runtime/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
```
sh ~/.vim_runtime/install.sh
```
## Usage

> **leader key : `,`**  

### Control TAB

> `leader` + tn : new tab  
> `leader` + tc : close tab  
> CTRL + z : previous tab  
> CTRL + x : next tab  

### Switch Pane

> CTRL + h/ j/ k/ l  

### Switch buffer

> `leader` + x : next  
> `leader` + z : previous  

### Enter INSERT mode

> `i` : insert before the cursor  
> `I` : insert before the first character of the line  
> `a` : insert after the cursor  
> `A` : inster after the last character of the line  
> `o` : begin a new line below the current line and insert  
> `O` : begin a new line above the current and insert  
> `gI`: insert at column 1 of the line  
> `gi`: insert where insert mode was last stopped  

#### Modifying text and enter INSERT mode

> `c` : delete text (and yank to the buffer) and enter insert mode  
> `cc`: delete the line and enter insert mode  
> `C` : delete until the end of the line and enter insert mode  
> `s` : delete a number of characters and enter insert mode  
> `S`: delete a number of line and enter insert mode  

#### Folding

> `zf` : create  
> `zo` : open  
> `zc` : close  
> `zd` : delete  
> `zO` : open all folder nested  
> `zM` : clse all folder nested  
> `zm` : increase fold level by one  
> `zr` : decrease fold lever by one  

### Function

> call ClassNew("FOO") : generate hpp/cpp FOO file  

### Exit INSERT mode

> kj  

### Fast saving

> `leader` + w  

### Center line in the screen

> `zz`  

### Move line

> CTRL + arrow key (up/down)  

### Multiple cursor without plugin

Basic usage:  
> /foo -> cgn -> `.` for change next occurence or `n` for skip and jump to the next word  
[Example from medium article](https://medium.com/@schtoeffel/you-don-t-need-more-than-one-cursor-in-vim-2c44117d51db)  

### Git (vim-fugitive & vim-flog)

> :Git <git-cmd>  
> :Flog or :Flogsplit  

### Visually select (with expand-region)

<!-- UNVALIDE ACTUALLY -->
> K : expand  
> J : shrink  

### Operate on text objects (target)

*Not yet configured*
> `dt` : delete until  
> `cw` : change word  
> `dw` : delete word

### Macros

> `q + id` : start recording  
> `@ + id` : play macro  

### ToDo

> Ctags (fn() gen/del with execptions)  
> Regex  
> Script to customize the theme (color of brackets)
> Asynchrun  

### Manuel change after install

> color of bracket: "/gruvbox-material.vim" @129: MatchParen: s:palette.orange  
> color of highlight: "/gruvbox-material.vim" @70: Search: s:palette.orange  
