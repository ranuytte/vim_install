#!/bin/bash
set -e

cd ~/.vim_runtime

echo 'set runtimepath+=~/.vim_runtime

source ~/.vim_runtime/vimrc/my_configs.vim
source ~/.vim_runtime/vimrc/filetypes.vim
source ~/.vim_runtime/vimrc/extended.vim
source ~/.vim_runtime/vimrc/basic.vim
source ~/.vim_runtime/vimrc/plugins_config.vim

let data_dir = "~/.vim_runtime"
if !isdirectory(glob(data_dir . "/plugged"))
	autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
	autocmd VimEnter * NERDTree
endif

' > ~/.vimrc

if [ -d ~/.vim ];
then
	if [ ! -f ~/.vim/coc-settings.json ];
	then
		cat coc-settings.json > ~/.vim/coc-settings.json
	fi
else
	mkdir ~/.vim
	cat coc-settings.json > ~/.vim/coc-settings.json
fi

echo "Done!"
