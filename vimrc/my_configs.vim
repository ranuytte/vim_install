"INSTALL PLUGIN (:PlugInstall)
call plug#begin('~/.vim_runtime/plugged')

Plug 'morhetz/gruvbox'
Plug 'sainnhe/gruvbox-material'
Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-fugitive'
Plug 'rbong/vim-flog', {'branch': 'v1'}
Plug 'preservim/nerdtree'
Plug 'tpope/vim-commentary'
Plug 'wellle/targets.vim'
Plug 'dense-analysis/ale'
Plug 'bfrg/vim-cpp-modern'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tmsvg/pear-tree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'sheerun/vim-polyglot'

call plug#end()

" Plug 'skywind3000/asyncrun.vim'
" Plug '42Paris/42header'

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","

" MAP <LEADER>rn FOR SEARCH/REPLACE
noremap <leader>s :call SearchReplaceCurrentFile()<CR>
noremap <leader>sg :call SearchReplaceRecursive()<CR>

" MAP BOOKMARKS
noremap <leader>m :call GotoBookmark()<CR>
noremap <leader>mn :call NewBookmark()<CR>
noremap <leader>ml :call DisplayBookmark()<CR>
noremap <leader>md :call DeleteBookmark()<CR>

" CONFIG TERMEDEBUG --UNUSED
" :packadd termdebug 
" let g:termdebug_wide = 100

" YANK TO CLIPBOARD
vnoremap <C-y> :w !xsel -i -b <CR><CR>

" GENERATE && UPDATE CTAGS --UNUSED
" au VimEnter * silent! !ctags -R
" au VimLeave	* silent! !rm tags

"DISPLAY RELATIVE NUMBER LINE
:set number
:set rnu

"ENABLE MOUSE
:set mouse=a

"HIGHLIGHT CURSORLINE
set cursorline
autocmd InsertEnter * highlight CursorLine guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE
autocmd InsertLeave * highlight CursorLine guifg=NONE guibg=NONE ctermfg=NONE ctermbg=237

"DISPLAY TAB && END OF LINE
set list
set listchars=eol:¬,tab:⍿·,trail:·

"MAP CTRL +Z/X FOR SIWTCH TAB
nnoremap <C-z> :tabprevious<CR>
nnoremap <C-x> :tabnext<CR>

"MAP CTRL +H/J/K/L FOR SWITCH PANE
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l

"AUTO-SAVE/AUTO-LOAD FOLD
au BufWinLeave *.* mkview
au BufWinEnter *.* if getcmdwintype() == '' && &buftype != 'quickfix' | silent loadview

" Use %% on the command line to expand to the path of the current file
cabbr <expr> %% expand('%:p:h')

" MAP CTRL + f FOR CALL FZF
nnoremap <silent> <C-f> :FZF<CR>

"MAP KJ FOR ESC
inoremap kj <esc>

let g:pear_tree_repeatable_expand = 0

"C++ CLASS GENERATOR: OPENING 2 NEW FILES
function! ClassNew(ClassName)
    "==================  editing source file =================================
    execute "vsp %:h/" . a:ClassName . ".cpp"
    "At this stage the autocomands for this filetype are done.
    "   example: inserting the header, and the ifndef... Then:
    :execute "normal! a#include \"" . a:ClassName . ".hpp\"\<cr>\<cr>"
    :execute "normal! a" . a:ClassName . "::" . a:ClassName ."(void)\<cr>{\<cr>"
    :execute "normal! a\<tab>return ;\<cr>"
    :execute "normal! a}\<cr>\<cr>"
    :execute "normal! a" . a:ClassName . "::" . a:ClassName ."(const " . a:ClassName ." &obj)\<cr>{\<cr>"
    :execute "normal! a\<tab>return ;\<cr>"
    :execute "normal! a}\<cr>\<cr>"
    :execute "normal! a" . a:ClassName . " &" . a:ClassName . "::operator=(const " . a:ClassName ." &obj)\<cr>{\<cr>"
    :execute "normal! a\<tab>return (*this);\<cr>"
    :execute "normal! a}\<cr>\<cr>"
    :execute "normal! a" . a:ClassName . "::~" . a:ClassName ."(void)\<cr>{\<cr>"
    :execute "normal! a\<tab>return ;\<cr>"
    :execute "normal! a}"
    "Comment this line if you dont want to save files straight away.
    :execute 'write'

    "==================  editing header file =================================
    execute "vsp %:h/" . a:ClassName . ".hpp"
    "At this stage the autocomands for this filetype are done.
    "   example: inserting the header, and the ifndef... Then:
	:execute "normal! a" . "#ifndef " . toupper(a:ClassName) . "_HPP\<cr>"
	:execute "normal! a" . "#define " . toupper(a:ClassName) . "_HPP\<cr>\<cr>"
	:execute "normal! a" . "class " . a:ClassName ."\<cr>{\<cr>"
    :execute "normal! a\<tab>public:\<cr>"
    :execute "normal! a\<tab>\<tab>explicit " . a:ClassName . "(void);\<cr>"
    :execute "normal! a\<tab>\<tab>~" . a:ClassName . "(void);\<cr>"
	:execute "normal! a\<tab>\<tab>" . a:ClassName . "(const " . a:ClassName . "&);\<cr>"
	:execute "normal! a\<tab>\<tab>" . a:ClassName . " &operator=(const " . a:ClassName . " &);\<cr>\<cr>"
    :execute "normal! a\<tab>protected:\<cr>"
	:execute "normal! a\<tab>\<tab>\<cr>"
    :execute "normal! a\<tab>private:\<cr>"
	:execute "normal! a\<tab>\<tab>\<cr>"
    :execute "normal! a};"
	:execute "normal! a\<cr>#endif"
    "Comment out this line if you dont want to start in insert mode
    " :startinsert!
    "Comment this line if you dont want to save files straight away.
    :execute 'write'
endfunction

function! SearchReplaceCurrentFile()
	silent :execute "vimgrep /" . expand('<cword>') . "/j %"
	call inputsave()
	let subWord = input('Replace: ')
	call inputrestore()
	silent :execute "cfdo " . "%s/" . expand('<cword>') . "/" . subWord . "/g" | :execute "update"
endfunction

function! SearchReplaceRecursive()
	silent :execute "vimgrep /" . expand('<cword>') . "/j **/*"
	call inputsave()
	let subWord = input('Replace: ')
	call inputrestore()
	silent :execute "cfdo " . "%s/" . expand('<cword>') . "/" . subWord . "/g"
	silent :execute "cfdo update"
	silent :execute "2,$cfdo tabclose"
endfunction

if !exists('g:BOOKMARKS')
  let g:BOOKMARKS = {}
endif

function! NewBookmark()
	let pos = getpos('.')
	let filename = expand('%')
	call inputsave()
	let name = input('Bookmark: ')
	call inputrestore()

	if filename != ''
		let g:BOOKMARKS[name] = [filename, pos]
	else
		echom "No file"
	endif
endfunction

function! GotoBookmark()
	call inputsave()
	let name = input('Bookmark: ')
	call inputrestore()

	if !has_key(g:BOOKMARKS, name)
		return
	endif

	let [filename, cursor] = g:BOOKMARKS[name]

	exe 'edit '.filename
	call setpos('.', cursor)
endfunction

function! DeleteBookmark()
	call inputsave()
	let name = input('Bookmark: ')
	call inputrestore()

	if !has_key(g:BOOKMARKS, name)
		return
	endif

	unlet g:BOOKMARKS[name]
	
endfunction

function! DisplayBookmark()
  let choices = []

  for [name, place] in items(g:BOOKMARKS)
    let [filename, cursor] = place

    call add(choices, {
          \ 'text':     name,
          \ 'filename': filename,
          \ 'lnum':     cursor[1],
          \ 'col':      cursor[2]
          \ })
  endfor

  call setqflist(choices)
  copen
endfunction
